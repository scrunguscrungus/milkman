﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Milkman
{
	public class RantConfig
	{
		public float flMutter;
		public float flVictimInvolved;
		public float flTransitional;

		public RantConfig( float mutterChance, float victimChance, float transitionalChance )
		{
			flMutter = mutterChance;
			flVictimInvolved = victimChance;
			flTransitional = transitionalChance;
		}
	}
	public class RantResult
	{
		public List<LineCode> Rant = new List<LineCode>();


		public void Add( LineCode code )
		{
			Rant.Add( code );
		}
		public void Clear()
		{
			Rant.Clear();
		}
		public override string ToString()
		{
			string rantString = string.Empty;

			foreach ( LineCode lcLine in Rant )
			{
				rantString += lcLine.Quote;
				if ( lcLine != Rant.Last() )
				{
					rantString += " ";
				}
			}

			return rantString;
		}
	}

	/*
		BoydRant
		Reimplementation of BoydRant.lua from Psychonauts
		Originally created by Anna Kipnis
	*/

	public class BoydRant
	{
		enum RantPiece
		{
			SingConsp,
			Consp,
			Loop,
			SemiT,
			Term,
			Linkback,
			VictimL,
			Victim,
			Trans,
			NonSeq,
			Mutter,
			AdvMutter
		}

		RantData Data;

		RantConfig config = new RantConfig( 0.5f, 0.5f, 0.5f );

		Random rng = new Random();

		public RantResult LastRant = new RantResult();
		Dictionary<RantPiece, LineCode> rTables = new Dictionary<RantPiece, LineCode>();

		int iSingular = 0;

		public BoydRant( RantData data, RantConfig config = null )
		{
			Data = data;
			if ( config != null )
				this.config = config;
		}
		public BoydRant( string data, RantConfig config = null )
		{
			Data = new RantData();
			Data.LoadData( File.ReadAllText(data) );
			if ( config != null )
				this.config = config;
		}

		public double random( double max = 1 )
		{
			return rng.NextDouble() * max;
		}
		public double random( double min, double max)
		{
			return rng.NextDouble() * ( max - min ) + min;
		}

		int RandInt(int iSeedLow = 0, int iSeedHigh = 0)
		{
			if ( iSeedLow != 0 && iSeedHigh != 0 )
			{
				return (int)Math.Floor( random( iSeedLow, iSeedHigh ) );
			}
			else if ( iSeedLow != 0 )
			{
				return (int)Math.Floor( random( iSeedLow ) );
			}
			else
			{
				return (int)Math.Floor( random( 1.9 ) );
			}
		}

		bool CoinFlip()
		{
			return RandInt() > 0;
		}

		LineCode[] GetTableForRantPiece( RantPiece type )
		{
			switch ( type )
			{
				case RantPiece.AdvMutter:
					return Data.AdvancedMutters;
				case RantPiece.Mutter:
					return Data.Mutters;
				case RantPiece.Consp:
					return Data.Conspirators;
				case RantPiece.SingConsp:
					return Data.SingularConspirators;
				case RantPiece.Loop:
					return Data.LoopingActions;
				case RantPiece.NonSeq:
					return Data.NonSequitors;
				case RantPiece.SemiT:
					return Data.SemiTerminals;
				case RantPiece.Term:
					return Data.Terminals;
				case RantPiece.Trans:
					return Data.Transitionals;
				case RantPiece.Victim:
					return Data.Victims;
				case RantPiece.VictimL:
					return Data.VictimLines;
				case RantPiece.Linkback:
					return Data.LinkBackLines;
				default:
					return null;
			}
		}

		void AddToRant( RantPiece type )
		{
			LineCode[] table = GetTableForRantPiece( type );

			LineCode line;

			do
			{
				line = table[ RandInt( table.Length ) ];
			}
			while ( rTables.ContainsKey( type ) && rTables[ type ].Quote == line.Quote && table.Length > 1 );

			LastRant.Add( line );

			rTables[ type ] = line;
		}

		public void Mutter( bool bAdvanced )
		{
			if ( random() <= config.flMutter )
			{
				bool bAdv = CoinFlip();

				if ( !bAdvanced || !bAdv )
					AddToRant( RantPiece.Mutter );
				else
					AddToRant( RantPiece.AdvMutter );
			}
		}

		public void NewRant()
		{
			LastRant.Clear();

			if ( CoinFlip() )
			{
				AddToRant( RantPiece.NonSeq );
				return;
			}

			StartRant();
		}

		public void StartRant()
		{
			if ( CoinFlip() )
			{
				AddToRant( RantPiece.SingConsp );
				iSingular++;
			}
			else
			{
				AddToRant( RantPiece.Consp );
				iSingular+=2;
			}

			Mutter( false );

			if ( CoinFlip() || iSingular == 1 )
			{
				LoopingAction();
			}
			else
			{
				TermOrSemi();
			}
		}

		public void LoopingAction()
		{
			AddToRant( RantPiece.Loop );

			Mutter( false );

			StartRant();
		}

		public void TermOrSemi()
		{
			iSingular = 0;

			Mutter( false );

			if ( CoinFlip() )
			{
				Terminal();
			}
			else
			{
				Semi();
			}
		}

		public void Terminal()
		{
			AddToRant( RantPiece.Term );

			Mutter( true );

			Linkback();
		}

		public void Semi()
		{
			AddToRant( RantPiece.SemiT );
			AddToRant( RantPiece.Consp );

			Mutter(false);

			Victim();
		}

		public void Linkback()
		{
			if ( CoinFlip() )
			{
				AddToRant( RantPiece.Linkback );

				Mutter( false );

				TermOrSemi();
			}
			else
			{
				Mutter( false );

				Transitional();
			}
		}

		public void Victim()
		{
			if ( random() <= config.flVictimInvolved )
			{
				AddToRant( RantPiece.VictimL );
				AddToRant( RantPiece.Victim );
			}

			Mutter( false );

			Linkback();
		}

		public void Transitional()
		{
			if ( random() <= config.flTransitional )
				AddToRant( RantPiece.Trans );

			//End of rant
		}
	}
}
