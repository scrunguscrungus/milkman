﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Milkman
{
	public class LineCode
	{
		public string Quote;
		public string Voice;
	}
	public class RantData
	{
		JObject json;

		public LineCode[] SingularConspirators;
		public LineCode[] Conspirators;
		public LineCode[] LoopingActions;
		public LineCode[] SemiTerminals;
		public LineCode[] Terminals;
		public LineCode[] LinkBackLines;
		public LineCode[] VictimLines;
		public LineCode[] Victims;
		public LineCode[] Transitionals;
		public LineCode[] NonSequitors;
		public LineCode[] Mutters;
		public LineCode[] AdvancedMutters;

		LineCode LoadLinecode( JObject linecode )
		{
			LineCode lc = new LineCode();

			lc.Quote = (string)linecode[ "quote" ];
			if ( linecode.ContainsKey( "voice" ) )
			{
				lc.Voice = (string)linecode[ "voice" ];
			}

			return lc;
		}

		LineCode[] LoadLineCodes( string key )
		{
			List<LineCode> codes = new List<LineCode>();

			JArray array = (JArray)json[ key ];
			foreach ( JObject linecode in array )
			{
				codes.Add( LoadLinecode( linecode ) );
			}

			return codes.ToArray();
		}

		public void LoadData( string json )
		{
			this.json = JObject.Parse(json);

			SingularConspirators = LoadLineCodes( "singularConspirators" );
			Conspirators = LoadLineCodes( "conspirators" );
			LoopingActions = LoadLineCodes( "loopingActions" );
			SemiTerminals = LoadLineCodes( "semiTerminals" );
			Terminals = LoadLineCodes( "terminals" );
			LinkBackLines = LoadLineCodes( "linkbackLines" );
			VictimLines = LoadLineCodes( "victimLines" );
			Victims = LoadLineCodes( "victims" );
			Transitionals = LoadLineCodes( "transitionals" );
			NonSequitors = LoadLineCodes( "nonsequitors" );
			Mutters = LoadLineCodes( "mutters" );
			AdvancedMutters = LoadLineCodes( "advancedMutters" );
		}
	}
}
