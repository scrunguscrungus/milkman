## Milkman

This library is a customisable, mostly accurate reimplmentation of the algorithm used to construct Boyd Cooper's insane ranting in the very excellent game Psychonauts.
It is based on code originally written by Anna Kipnis for the game.

The code isn't pretty, though this project was mainly for fun anyway.

### Differences from the original algorithm
 - Does not infinitely loop, instead constructs a singular rant and ceases where the rant would normally loop back to the start
 - Has a chance to just rant a non-sequitor instead of a full rant. In the original code, a non-sequitor is always spoken when a transitional is considered.

### Usage
I have no idea what the use case for this library would really be, but regardless:

#### RantData
Before using the library, you need to have a RantData JSON file. A default one, `boyd.json`, has been provided which shows how these files are structures.
Every array is required, however only the `quote` field is required for each LineCode. `voice` may be provided to tie the linecode to a sound file for audio-based implementations. `og_alt` and `voice_alt` are both deprecated and will have no effect.

Each array's purpose is as follows:
 - **singularConspirators**: singular entities that will be regarded as conspiring against the ranter. e.g. `the kid with the goggles`, `a secret doomsday cult`, `big oil`
 - **conspirators**: Conspirators that can be regarded as a group of entities. e.g. `the squirrels`, `the psychowhatsits`, `the cows`
 - **loopingActions**: Lines that can be chained infinitely to keep bringing more conspirators into the rant. e.g. `and`, `or else, maybe`, `with the backing of`
 - **semiTerminals**: Accusatory lines that can be used to bring an additional conspirator into the rant. e.g. `sold their soul to`, `went to the prom with`, `are working for`
 - **terminals**: Accusatory lines that do NOT bring additional conspirators into the rant. e.g. `won't stop visiting me.`, `have everyone fooled.`, `are not to be trusted.`
 - **linkbackLines**: Lines that can be used to extend the rant by bridging to additional terminals or semi-terminals, i.e. `and they obviously`, `and, let's just say for now that they`, `but they can't hide that they`
 - **victimLines**: Lines that can be used to bring a victim into the rant, used after semi-terminals. e.g. `to keep down`, `to get`, `and who wins? Them. Who loses?`
 - **victims**: Victims that will be used as the subject of the victim lines. i.e. `all of us.`, `my hooch.`, `the water supply.`,
 - **transitionals**: Lines that can be used to finalise the rant. i.e. `How long do they think they can hide that?`, `f they find out I know this stuff, I'm dead`, `...right under people's noses!`
 - **nonsequitors**: Non-sequitors that will occasionally be used instead of a full rant. i.e. `I have to get rid of some of this stuff!`, `But when that happens, they turn it into CHOCOLATE milk, and nobody can tell the difference!`, `They think the windows are tinted, but they ain't tinted nearly enough!`
 - **mutters**: Random muttering that will be interspersed with the elements of the rant. i.e. `...uh...`, `...heh heh...`, `...er...`
 - **advancedMutters**: Similar to mutters, used less frequently and only in specific places. i.e. `...wait...` `...what?...`, `...okay, okay, but...`

#### Code
Once you have an appropriate set of rant data, you can begin code implementation.

Create an instance of `BoydRant`, which will generate the rant. In the constructor, you should provide either a `RantData` instance or a path to a JSON file from which rant data can be parsed. You can also optionally provide a `RantConfig` instance which can be used to configure the chances of a mutter, victim involvement, and transitionals. Not providing one will use the default values from the original game (50% for each).

Call `NewRant` to generate a rant. You can access the resulting rant from the `LastRant` field in `BoydRant`. The result will be of type `RantResult`, which has its own `ToString` method that will return a text version of the rant based on the `quote` fields in your rant data. If you need to directly access the array of linecodes to access other information, use the `Rant` field.

I understand this implementation is a little strange (`NewRant` should probably just return the rant directly), but I don't really expect this library to be used for much and I made it for fun, so I didn't really decide to make it better, but I may do so anyway later down the line.
